document.querySelector('#btnThemSinhVien').onclick = function () {
    //Lấy thông tin sinh viên từ người dùng nhập vào(input)
    var sv = new SinhVien();
    sv.maSinhVien = document.querySelector('#maSinhVien').value;
    sv.tenSinhVien = document.querySelector('#tenSinhVien').value;
    sv.loaiSinhVien = document.querySelector('#loaiSinhVien').value;
    sv.diemRenLuyen = document.querySelector('#diemRenLuyen').value;
    sv.email = document.querySelector('#email').value;
    sv.soDienThoai = document.querySelector('#soDienThoai').value;

    console.log(sv);

    //Tạo ra thẻ tr khi mỗi lần người dùng click vào thêm sinh viên
    var trSinhVien = document.createElement('tr');

    //Tạo ra các thẻ td
    var tdMaNhanVien = document.createElement('td');
    tdMaNhanVien.innerHTML = sv.maSinhVien;

    var tdTenSinhVien = document.createElement('td');
    tdTenSinhVien.innerHTML = sv.tenSinhVien;

    var tdEmail = document.createElement('td');
    tdEmail.innerHTML = sv.email;

    var tdSoDienThoai = document.createElement('td');
    tdSoDienThoai.innerHTML = sv.soDienThoai;

    var tdLoaiSinhVien = document.createElement('td');
    tdLoaiSinhVien.innerHTML = sv.loaiSinhVien;
    //append td vào tr
    trSinhVien.appendChild(tdMaNhanVien);
    trSinhVien.appendChild(tdTenSinhVien);
    trSinhVien.appendChild(tdEmail);
    trSinhVien.appendChild(tdSoDienThoai);
    trSinhVien.appendChild(tdLoaiSinhVien);
    //append tr vào tbody
    document.querySelector('#tblSinhVien').appendChild(trSinhVien);

}